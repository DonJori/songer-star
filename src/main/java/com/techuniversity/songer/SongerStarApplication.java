package com.techuniversity.songer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SongerStarApplication {

	public static void main(String[] args) {
		SpringApplication.run(SongerStarApplication.class, args);
	}

}
