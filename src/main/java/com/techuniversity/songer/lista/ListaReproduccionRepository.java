package com.techuniversity.songer.lista;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ListaReproduccionRepository extends MongoRepository<ListaReproduccionModel,Long> {
}
