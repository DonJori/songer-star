package com.techuniversity.songer.lista;


import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.lang.NonNull;

@Document(collection="listaReproduccion")
public class ListaReproduccionModel {

    @Transient
    public static final String SEQUENCE_NAME = "listaRepro_sequence";
    @Id
    private Long idLista;
    private Long idUsuario;
    private String descripcion;

    public ListaReproduccionModel() {
    }

    public ListaReproduccionModel(@NonNull Long idLista, Long idUsuario, String descripcion) {
        this.idLista = idLista;
        this.idUsuario = idUsuario;
        this.descripcion = descripcion;
    }

    @NonNull
    public Long getIdLista() {
        return idLista;
    }

    public void setIdLista(@NonNull Long idLista) {
        this.idLista = idLista;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
