package com.techuniversity.songer.lista;

import com.techuniversity.songer.cancion.CancionModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/listaReproduccion")
public class ListaReproduccionController {
    @Autowired
    ListaReproduccionService listaReproduccionService;
    @GetMapping("/")
    public List<ListaReproduccionDTO> getListas(@RequestParam(defaultValue = "-1") String page){
        int iPage = Integer.parseInt(page);
        if (iPage == -1){
            return listaReproduccionService.findAll();
        }else {
            return listaReproduccionService.findPaginado(iPage);
        }
    }

    //Consultar Listas por IdUsuario
    @GetMapping("listas/{idUsuario}/")
    public List<ListaReproduccionDTO> buscarListasReproduccion(@PathVariable long idUsuario)  {
        return listaReproduccionService.getListasReproduccion(idUsuario);
    }

    @GetMapping("/{id}")
    public ListaReproduccionDTO getListaId(@PathVariable long id) {
        return listaReproduccionService.findById(id);
    }

    @PostMapping("/")
    public String insertLista(@RequestBody ListaReproduccionModel listaReproduccionModel) {
        try {
            listaReproduccionService.insert(listaReproduccionModel);
            return "OK";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    //Borra lista por descripcion e Id
    @DeleteMapping("/{idUsuario}")
    public String borrarLista(@PathVariable long idUsuario, @RequestHeader String contrasenyaUsuario,@RequestHeader String descripcion) {
        try {
            listaReproduccionService.deleteListaReproduccion(idUsuario,descripcion);
            return "OK";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @PutMapping("/")
    public void updateLista(@RequestBody ListaReproduccionModel lista) {
        listaReproduccionService.save(lista);
    }

    //borra todas las listas de reproduccion
    @DeleteMapping("/")
    public boolean deleteLista(@RequestBody ListaReproduccionModel lista) {
        return listaReproduccionService.deleteLista(lista);
    }
} 

