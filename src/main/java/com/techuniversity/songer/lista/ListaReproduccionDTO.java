package com.techuniversity.songer.lista;

import com.techuniversity.songer.cancion.CancionDTO;

import java.util.List;

public class ListaReproduccionDTO {
    private long idLista;
    private long idUsuario;
    private String descripcion;
    private List<CancionDTO> canciones;

    public ListaReproduccionDTO() {}

    public long getIdLista() {
        return idLista;
    }

    public void setIdLista(long idLista) {
        this.idLista = idLista;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<CancionDTO> getCanciones() {
        return canciones;
    }

    public void setCanciones(List<CancionDTO> canciones) {
        this.canciones = canciones;
    }
}
