package com.techuniversity.songer.lista;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import com.techuniversity.songer.cancion.CancionDTO;
import com.techuniversity.songer.cancion.CancionModel;
import com.techuniversity.songer.cancion.CancionService;
import com.techuniversity.songer.listarelacionada.ListaRelacionadaModel;
import com.techuniversity.songer.listarelacionada.ListaRelacionadaService;
import com.techuniversity.songer.usuario.UsuarioModel;
import com.techuniversity.songer.utils.SequenceGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.bson.Document;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ListaReproduccionService {
    @Autowired
    ListaReproduccionRepository listaReproduccionRepository;
    @Autowired
    SequenceGeneratorService sequenceGeneratorService;
    @Autowired
    ListaRelacionadaService listaRelacionadaService;
    @Autowired
    CancionService cancionService;

    private static MongoCollection<Document> getListaRCollection() {
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder().applyConnectionString(cs).retryWrites(true).build();
        MongoClient cliente = MongoClients.create(settings);
        MongoDatabase database = cliente.getDatabase("songerStar");
        return database.getCollection("listaReproduccion");
    }

   //Obtiene Listas de Reproduccion por idUsuario
    public List<ListaReproduccionDTO> getListasReproduccion(long idUsuario) {
        MongoCollection<Document> lRepro = getListaRCollection();
        Document query = new Document("idUsuario", new Document("$eq", idUsuario));
        List<Document> ocurrenciasIdUsuario = lRepro.find(query).into(new ArrayList<Document>());

        List<ListaReproduccionDTO> listaReproduccionDTO = getListaReproduccionDTO(ocurrenciasIdUsuario);
        return listaReproduccionDTO;
    }

    private List<ListaReproduccionDTO> getListaReproduccionDTO(List<Document> ocurrenciasIdUsuario) {
        List<ListaReproduccionDTO> listaReproduccionDTO = new ArrayList<>();

        for (Document document : ocurrenciasIdUsuario) {
            ListaReproduccionModel dummy = crearModelo(document);

            // Recoge las relaciones de lista de reproduccion y cancion
            List<ListaRelacionadaModel> listaRelacionesModel = listaRelacionadaService.filtrarPoridLista(dummy.getIdLista());

            List<CancionDTO> listaCancionesDTO = new ArrayList<>();

            for (ListaRelacionadaModel relacionModel : listaRelacionesModel) {
                CancionDTO cancionModel = cancionService.getCancion(relacionModel.getIdCancion());
                listaCancionesDTO.add(cancionModel);
            }

            ListaReproduccionDTO listaDTO = transformarDTO(dummy, listaCancionesDTO);

            listaReproduccionDTO.add(listaDTO);
        }

        return listaReproduccionDTO;
    }

    private static ListaReproduccionModel crearModelo(Document document) {
        ListaReproduccionModel listaRepro = new ListaReproduccionModel();
        if (document.get("_id") != null) {
            long idLista = ((Number)document.get("_id")).longValue();
            listaRepro.setIdLista(idLista);
        }
        if (document.get("idUsuario") != null) {
            long idUsuario = ((Number)document.get("idUsuario")).longValue();
            listaRepro.setIdUsuario(idUsuario);
        }
        if (document.get("descripcion") != null) {
            String descripcion = ((String)document.get("descripcion"));
            listaRepro.setDescripcion(descripcion);
        }
        return listaRepro;
    }

    private static ListaReproduccionDTO transformarDTO(ListaReproduccionModel listaReproduccionModel, List<CancionDTO> listaCanciones) {
        ListaReproduccionDTO listaReproduccionDTO = new ListaReproduccionDTO();

        listaReproduccionDTO.setIdLista(listaReproduccionModel.getIdLista());
        listaReproduccionDTO.setIdUsuario(listaReproduccionModel.getIdUsuario());
        listaReproduccionDTO.setDescripcion(listaReproduccionModel.getDescripcion());
        listaReproduccionDTO.setCanciones(listaCanciones);

        return listaReproduccionDTO;
    }

    public List<ListaReproduccionDTO> findAll() {

        List<ListaReproduccionModel> listaReproduccionModel = listaReproduccionRepository.findAll();

        List<ListaReproduccionDTO> listaDTO = new ArrayList<>();

        for (ListaReproduccionModel listaReproduccion : listaReproduccionModel) {
            ListaReproduccionDTO listaReproduccionDTO = getListaReproduccionDTO(listaReproduccion);
            listaDTO.add(listaReproduccionDTO);
        }

        return listaDTO;
    }

    private ListaReproduccionDTO getListaReproduccionDTO(ListaReproduccionModel listaReproduccionModel) {

        List<CancionDTO> listaCancionesDTO = new ArrayList<>();

        List<ListaRelacionadaModel> listaRelaciones = listaRelacionadaService.filtrarPoridLista(listaReproduccionModel.getIdLista());

        for (ListaRelacionadaModel relacion : listaRelaciones) {
            CancionDTO cancionModel = cancionService.getCancion(relacion.getIdCancion());
            listaCancionesDTO.add(cancionModel);
        }

        ListaReproduccionDTO listaReproduccionDTO = transformarDTO(listaReproduccionModel, listaCancionesDTO);
        return listaReproduccionDTO;
    }

    public ListaReproduccionDTO findById(Long id) {

        Optional<ListaReproduccionModel> listaReproduccionModel = listaReproduccionRepository.findById(id);

        if (listaReproduccionModel.isPresent()) {
            return getListaReproduccionDTO(listaReproduccionModel.get());
        }

        return null;
    }

    public ListaReproduccionModel save(ListaReproduccionModel listaReproduccion) {

        return listaReproduccionRepository.save(listaReproduccion);
    }

    public ListaReproduccionModel insert(ListaReproduccionModel listaReproduccion) throws Exception {

        ListaReproduccionModel nuevaListaRepro = new ListaReproduccionModel(
                sequenceGeneratorService.generateSequence(ListaReproduccionModel.SEQUENCE_NAME),
                listaReproduccion.getIdUsuario(),
                listaReproduccion.getDescripcion());

        return listaReproduccionRepository.save(nuevaListaRepro);
    }


    public boolean deleteLista(ListaReproduccionModel lista){
        try{
            listaReproduccionRepository.delete(lista);
            return true;
        }catch (Exception ex){
            return false;
        }
    }
    public List<ListaReproduccionDTO> findPaginado(int page){
        Pageable pageable = PageRequest.of(page,3);
        Page<ListaReproduccionModel> pages = listaReproduccionRepository.findAll(pageable);
        List<ListaReproduccionModel> listas = pages.getContent();

        List<ListaReproduccionModel> listaReproduccionModel = listaReproduccionRepository.findAll();

        List<ListaReproduccionDTO> listaDTO = new ArrayList<>();

        for (ListaReproduccionModel listaReproduccion : listaReproduccionModel) {
            ListaReproduccionDTO listaReproduccionDTO = getListaReproduccionDTO(listaReproduccion);
            listaDTO.add(listaReproduccionDTO);
        }

        return listaDTO;
    }

    public void deleteListaReproduccion(long idUsuario,String descripcion) {
        // TODO: Borrar en cascada
        /*List<ListaReproduccionModel> listaRepro = getListasReproduccion(idUsuario);
         for (ListaReproduccionModel dummy: listaRepro) {
             if (dummy.getDescripcion().equals(descripcion));
             listaReproduccionRepository.delete(dummy);
         }*/
    }
}
