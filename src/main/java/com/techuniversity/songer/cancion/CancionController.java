package com.techuniversity.songer.cancion;

import com.techuniversity.songer.usuario.UsuarioModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/cancion")
public class CancionController {
    @Autowired
    CancionService cancionService;

    @GetMapping("/canciones")
    public List<CancionDTO> getCanciones(@RequestParam(defaultValue =  "-1") String page) {
        int iPage = Integer.parseInt(page);
        if (iPage == -1) {
            return cancionService.findAll();
        } else {
            return cancionService.findPaginado(iPage);
        }
    }

    @GetMapping("/{id}")
    public CancionDTO getCancion(@PathVariable long id) {
        return cancionService.getCancion(id);
    }

    @GetMapping("/genero/{genero}")
    public List<CancionDTO> getCancionesPorGenero(@PathVariable String genero) {
        return cancionService.buscarPorGenero(genero);
    }

    @GetMapping("/artista/{artista}")
    public List<CancionDTO> getCancionesPorArtista(@PathVariable String artista) {
        return cancionService.buscarPorArtista(artista);
    }

    @GetMapping("/album/{album}")
    public List<CancionDTO> getCancionesPorAlbum(@PathVariable String album) {
        return cancionService.buscarPorAlbum(album);
    }

    @PostMapping("/cancion")
    public String insertCancion(@RequestBody CancionModel cancionModel) {
        try {
            cancionService.insertarCancion(cancionModel);
            return "OK";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @PutMapping("/")
    public void updateCancion(@RequestBody CancionModel cancion) {
        cancionService.update(cancion);
    }

    @DeleteMapping("/")
    public boolean deleteCancion(@RequestBody CancionModel cancion) {
        return cancionService.deleteProducto(cancion);
    }

    @GetMapping("/filtro")
    public List getServicios(@RequestBody String filtro) {
        return cancionService.getFiltrados(filtro);
    }

    @PostMapping("/canciones")
    public void insertCanciones(@RequestBody String canciones) {
        try {
            cancionService.insertBatch(canciones);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

}
