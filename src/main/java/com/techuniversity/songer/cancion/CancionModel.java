package com.techuniversity.songer.cancion;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "canciones")
public class CancionModel {

    @Transient
    public static final String SEQUENCE_NAME = "canciones_sequence";

    @Id
    private Long id;
    private String nombre;
    private String album;
    private String artista;
    private String genero;

    public CancionModel() {
    }

    public CancionModel(Long id, String nombre, String album, String artista, String genero) {
        this.id = id;
        this.nombre = nombre;
        this.album = album;
        this.artista = artista;
        this.genero = genero;
    }

    public static String getSequenceName() {
        return SEQUENCE_NAME;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtista() {
        return artista;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
}
