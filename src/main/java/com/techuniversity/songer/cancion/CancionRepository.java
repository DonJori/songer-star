package com.techuniversity.songer.cancion;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CancionRepository extends MongoRepository<CancionModel, Long> {
}
