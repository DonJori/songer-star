package com.techuniversity.songer.cancion;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import com.techuniversity.songer.usuario.UsuarioModel;
import com.techuniversity.songer.utils.SequenceGeneratorService;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CancionService {

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;
    @Autowired
    CancionRepository cancionRepository;

    private static MongoCollection<Document> getCancionesCollection() {
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder().applyConnectionString(cs).retryWrites(true).build();
        MongoClient cliente = MongoClients.create(settings);
        MongoDatabase database = cliente.getDatabase("songerStar");

        return database.getCollection("canciones");
    }

    public List<CancionDTO> findAll() {
        List<CancionModel> cancionesModel = cancionRepository.findAll();

        List<CancionDTO> cancionesDTO = new ArrayList<>();

        for (CancionModel cancionModel : cancionesModel) {
            CancionDTO cancionDTO = transformarDTO(cancionModel);
            cancionesDTO.add(cancionDTO);
        }

        return cancionesDTO;
    }

    public CancionDTO update(CancionModel cancion) {

        CancionModel dummy = cancionRepository.save(cancion);
        CancionDTO cancionDTO = transformarDTO(dummy);
        return cancionDTO;
    }

    public boolean deleteProducto(CancionModel cancion) {
        try {
            cancionRepository.delete(cancion);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public List<CancionDTO> findPaginado(int page) {
        Pageable pageable = PageRequest.of(page, 3);
        Page<CancionModel> pages = cancionRepository.findAll(pageable);
        List<CancionModel> canciones = pages.getContent();

        List<CancionDTO> cancionesDTO = new ArrayList<>();

        for (CancionModel cancionModel : canciones) {
            cancionesDTO.add(transformarDTO(cancionModel));
        }

        return cancionesDTO;
    }

    public static List<CancionModel> getFiltrados(String cadenaFiltro) {
        MongoCollection<Document> canciones = getCancionesCollection();
        List lista = new ArrayList();
        Document docFiltro = Document.parse(cadenaFiltro);
        FindIterable<Document> iterDoc = canciones.find(docFiltro);
        Iterator iterador = iterDoc.iterator();
        while (iterador.hasNext()) {
            lista.add(iterador.next());
        }
        return lista;
    }

    public void insertBatch(String cadenaCanciones) throws Exception {
        Document doc = Document.parse(cadenaCanciones);
        List<Document> lstCanciones = doc.getList("canciones", Document.class);
        for (Document cancion: lstCanciones) {
            CancionModel cancionModel =  crearModelo(cancion);
            cancionRepository.save(cancionModel);
        }

    }

    public void insertarCancion(CancionModel cancionModel) throws Exception {

        CancionModel nuevaCancion = new CancionModel(
                sequenceGeneratorService.generateSequence(CancionModel.SEQUENCE_NAME),
                cancionModel.getNombre(),
                cancionModel.getAlbum(),
                cancionModel.getArtista(),
                cancionModel.getGenero());

        cancionRepository.save(nuevaCancion);
    }

    public List<CancionDTO> buscarPorCancion(String nombreCancion) {
        MongoCollection<Document> canciones = getCancionesCollection();

        Document query = new Document("nombre", new Document("$eq", nombreCancion));
        List<Document> ocurrenciasCanciones = canciones.find(query).into(new ArrayList<Document>());

        List<CancionDTO> listaCanciones = new ArrayList<>();

        for (Document document : ocurrenciasCanciones) {
            CancionModel dummy = crearModelo(document);
            CancionDTO cancionDTO = transformarDTO(dummy);
            listaCanciones.add(cancionDTO);
        }

        return listaCanciones;
    }

    public List<CancionDTO> buscarPorGenero(String genero) {
        MongoCollection<Document> canciones = getCancionesCollection();

        Document query = new Document("genero", new Document("$eq", genero));
        List<Document> ocurrenciasCanciones = canciones.find(query).into(new ArrayList<Document>());

        List<CancionDTO> listaCanciones = new ArrayList<>();

        for (Document document : ocurrenciasCanciones) {
            CancionModel dummy = crearModelo(document);
            CancionDTO cancionDTO = transformarDTO(dummy);
            listaCanciones.add(cancionDTO);
        }

        return listaCanciones;
    }

    public List<CancionDTO> buscarPorArtista(String artista) {
        MongoCollection<Document> canciones = getCancionesCollection();

        Document query = new Document("artista", new Document("$eq", artista));
        List<Document> ocurrenciasCanciones = canciones.find(query).into(new ArrayList<Document>());

        List<CancionDTO> listaCanciones = new ArrayList<>();

        for (Document document : ocurrenciasCanciones) {
            CancionModel dummy = crearModelo(document);
            CancionDTO cancionDTO = transformarDTO(dummy);
            listaCanciones.add(cancionDTO);
        }

        return listaCanciones;
    }

    public List<CancionDTO> buscarPorAlbum(String album) {
        MongoCollection<Document> canciones = getCancionesCollection();

        Document query = new Document("album", new Document("$eq", album));
        List<Document> ocurrenciasCanciones = canciones.find(query).into(new ArrayList<Document>());

        List<CancionDTO> listaCanciones = new ArrayList<>();

        for (Document document : ocurrenciasCanciones) {
            CancionModel dummy = crearModelo(document);
            CancionDTO cancionDTO = transformarDTO(dummy);
            listaCanciones.add(cancionDTO);
        }

        return listaCanciones;
    }

    private static CancionModel crearModelo(Document document) {
        CancionModel cancionModel = new CancionModel();

        if (document.get("_id") != null) {
            long idCancion = ((Number)document.get("_id")).longValue();
            cancionModel.setId(idCancion);
        }

        if (document.get("nombre") != null) {
            cancionModel.setNombre((String)document.get("nombre"));
        }

        if (document.get("album") != null) {
            cancionModel.setAlbum((String)document.get("album"));
        }

        if (document.get("artista") != null) {
            cancionModel.setArtista((String)document.get("artista"));
        }

        if (document.get("genero") != null) {
            cancionModel.setGenero((String)document.get("genero"));
        }

        return cancionModel;
    }

    private static CancionDTO transformarDTO(CancionModel cancionModel) {
        CancionDTO cancionDTO = new CancionDTO();

        cancionDTO.setIdCancion(cancionModel.getId());
        cancionDTO.setNombre(cancionModel.getNombre());
        cancionDTO.setAlbum(cancionModel.getAlbum());
        cancionDTO.setArtista(cancionModel.getArtista());
        cancionDTO.setGenero(cancionModel.getGenero());

        return cancionDTO;
    }

    public CancionDTO getCancion(long id) {
        return transformarDTO(cancionRepository.findById(id).get());
    }

}
