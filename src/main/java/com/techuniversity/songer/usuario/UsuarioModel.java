package com.techuniversity.songer.usuario;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "usuarios")
public class UsuarioModel {

    @Transient
    public static final String SEQUENCE_NAME = "usuarios_sequence";

    @Id
    private long idUsuario;
    private String nombreUsuario;
    private String contrasenyaUsuario;
    private String apodoUsuario;

    public UsuarioModel() {}

    public UsuarioModel(long idUsuario, String nombreUsuario, String contrasenyaUsuario, String apodoUsuario) {
        this.idUsuario = idUsuario;
        this.nombreUsuario = nombreUsuario;
        this.contrasenyaUsuario = contrasenyaUsuario;
        this.apodoUsuario = apodoUsuario;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getContrasenyaUsuario() {
        return contrasenyaUsuario;
    }

    public void setContrasenyaUsuario(String contrasenyaUsuario) {
        this.contrasenyaUsuario = contrasenyaUsuario;
    }

    public String getApodoUsuario() {
        return apodoUsuario;
    }

    public void setApodoUsuario(String apodoUsuario) {
        this.apodoUsuario = apodoUsuario;
    }
}
