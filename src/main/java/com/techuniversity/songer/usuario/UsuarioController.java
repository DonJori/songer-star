package com.techuniversity.songer.usuario;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @GetMapping("/usuarios")
    public List<UsuarioDTO> getUsuarios(@RequestParam(defaultValue = "-1") String page) {
        int iPage = Integer.parseInt(page);

        if (iPage == -1) {
            return usuarioService.getUsuarios();
        }

        return usuarioService.getUsuarios(iPage);
    }

    @GetMapping("/{idUsuario}")
    public ResponseEntity<UsuarioDTO> getUsuario(@PathVariable long idUsuario) {

        UsuarioDTO usuarioDTO = usuarioService.getUsuario(idUsuario);

        if (usuarioDTO != null) {
            return new ResponseEntity<UsuarioDTO>(usuarioDTO, HttpStatus.OK){};
        }

        return ResponseEntity.notFound().build();
    }

    @GetMapping("/")
    public String loginUsuario(@RequestHeader String nombreUsuario, @RequestHeader String contrasenyaUsuario) {
        // TODO: Implementar
        return "OK";
    }

    @PostMapping("/")
    public String insertUsuario(@RequestBody UsuarioModel usuarioModel) {
        try {
            usuarioService.insertarUsuario(usuarioModel);
            return "OK";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @DeleteMapping("/{idUsuario")
    public String deleteUsuario(@PathVariable long idUsuario, @RequestHeader String contrasenyaUsuario) {
        try {
            usuarioService.deleteUsuario(idUsuario, contrasenyaUsuario);
            return "OK";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @PutMapping("/{idUsuario}")
    public String updateUsuario(@PathVariable long idUsuario, @RequestHeader String contrasenyaUsuario, @RequestHeader String nuevaContrasenya) {
        try {
            usuarioService.updateUsuario(idUsuario, contrasenyaUsuario, nuevaContrasenya);
            return "OK";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

}
