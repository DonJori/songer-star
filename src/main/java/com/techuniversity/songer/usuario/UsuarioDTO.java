package com.techuniversity.songer.usuario;

import com.techuniversity.songer.lista.ListaReproduccionDTO;

import java.util.List;

public class UsuarioDTO {
    private long idUsuario;
    private String nombreUsuario;
    private String apodoUsuario;
    private List<ListaReproduccionDTO> listasReproduccion;

    public UsuarioDTO() {}

    public UsuarioDTO(long idUsuario, String nombreUsuario, String apodoUsuario) {
        this.idUsuario = idUsuario;
        this.nombreUsuario = nombreUsuario;
        this.apodoUsuario = apodoUsuario;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombreUsuario;
    }

    public void setNombre(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getApodo() {
        return apodoUsuario;
    }

    public void setApodo(String apodoUsuario) {
        this.apodoUsuario = apodoUsuario;
    }

    public List<ListaReproduccionDTO> getListasReproduccion() {
        return listasReproduccion;
    }

    public void setListasReproduccion(List<ListaReproduccionDTO> listasReproduccion) {
        this.listasReproduccion = listasReproduccion;
    }
}
