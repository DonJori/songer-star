package com.techuniversity.songer.usuario;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.techuniversity.songer.lista.ListaReproduccionDTO;
import com.techuniversity.songer.lista.ListaReproduccionModel;
import com.techuniversity.songer.lista.ListaReproduccionService;
import com.techuniversity.songer.utils.SequenceGeneratorService;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;
    @Autowired
    UsuarioRepository usuarioRepository;
    @Autowired
    ListaReproduccionService listaReproduccionService;

    public static MongoCollection<Document> getUsuariosCollection() {
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder().applyConnectionString(cs).retryWrites(true).build();
        MongoClient cliente = MongoClients.create(settings);
        MongoDatabase database = cliente.getDatabase("songerStar");

        return database.getCollection("usuarios");
    }

    public List<UsuarioModel> buscarPorNombre(String nombreUsuario) {
        MongoCollection<Document> usuarios = getUsuariosCollection();

        Document query = new Document("nombreUsuario", new Document("$eq", nombreUsuario));
        ArrayList<Document> ocurrenciasUsuarios = usuarios.find(query).into(new ArrayList<Document>());

        ArrayList<UsuarioModel> listaUsuarios = new ArrayList<>();

        for (Document document : ocurrenciasUsuarios) {
            UsuarioModel dummy = crearModelo(document);
            listaUsuarios.add(dummy);
        }

        return listaUsuarios;
    }

    private static UsuarioModel crearModelo(Document document) {
        UsuarioModel usuarioModel = new UsuarioModel();

        if (document.get("_id") != null) {
            long idUsuario = ((Number)document.get("_id")).longValue();
            usuarioModel.setIdUsuario(idUsuario);
        }

        if (document.get("nombreUsuario") != null) {
            usuarioModel.setNombreUsuario((String)document.get("nombreUsuario"));
        }

        if (document.get("apodoUsuario") != null) {
            usuarioModel.setApodoUsuario((String)document.get("apodoUsuario"));
        }

        return usuarioModel;
    }

    public UsuarioDTO getUsuario(long idUsuario) {
        // TODO: Seleccionar en cascada
        Optional<UsuarioModel> usuarioModel = usuarioRepository.findById(idUsuario);

        if (usuarioModel.isPresent()) {
            return convertirDTO(usuarioModel.get());
        }

        return null;
    }

    private UsuarioDTO convertirDTO(UsuarioModel usuarioModel) {
        UsuarioDTO usuarioDTO = new UsuarioDTO();

        usuarioDTO.setIdUsuario(usuarioModel.getIdUsuario());
        usuarioDTO.setNombre(usuarioModel.getNombreUsuario());
        usuarioDTO.setApodo(usuarioModel.getApodoUsuario());

        List<ListaReproduccionDTO> listasReproduccionDTO = listaReproduccionService.getListasReproduccion(usuarioModel.getIdUsuario());
        usuarioDTO.setListasReproduccion(listasReproduccionDTO);

        return usuarioDTO;
    }

    public List<UsuarioDTO> getUsuarios() {
        // TODO: Seleccionar en cascada
        List<UsuarioModel> listaUsuariosModel = usuarioRepository.findAll();
        List<UsuarioDTO> listaUsuariosDTO = new ArrayList<>();

        for (UsuarioModel usuarioModel : listaUsuariosModel) {
            listaUsuariosDTO.add(convertirDTO(usuarioModel));
        }

        return listaUsuariosDTO;
    }

    public List<UsuarioDTO> getUsuarios(int page) {
        // TODO: Seleccionar en cascada
        Pageable pageable = PageRequest.of(page, 3);
        Page<UsuarioModel> pages = usuarioRepository.findAll(pageable);
        List<UsuarioModel> listaUsuariosModel = pages.getContent();

        List<UsuarioDTO> listaUsuariosDTO = new ArrayList<>();

        for (UsuarioModel usuarioModel : listaUsuariosModel) {
            listaUsuariosDTO.add(convertirDTO(usuarioModel));
        }

        return listaUsuariosDTO;
    }

    public void insertarUsuario(UsuarioModel usuarioModel) throws Exception {

        List<UsuarioModel> resultado = buscarPorNombre(usuarioModel.getNombreUsuario());

        if (resultado != null && !resultado.isEmpty()) {
            throw new Exception("Ya existe un usuario con el mismo nombre, por favor inserte uno diferente.");
        }

        UsuarioModel nuevoUsuario = new UsuarioModel(
                sequenceGeneratorService.generateSequence(UsuarioModel.SEQUENCE_NAME),
                usuarioModel.getNombreUsuario(),
                usuarioModel.getContrasenyaUsuario(),
                usuarioModel.getApodoUsuario());

        usuarioRepository.save(nuevoUsuario);
    }

    public Optional<UsuarioModel> loginUsuario(long idUsuario, String contrasenyaUsuario) {
        return comprobarUsuario(idUsuario, contrasenyaUsuario);
    }

    private Optional<UsuarioModel> comprobarUsuario(long idUsuario, String contrasenyaUsuario) {

        // TODO: Devolver token de seguridad
        Optional<UsuarioModel> usuarioModel = usuarioRepository.findById(idUsuario);

        if (usuarioModel.isPresent() && usuarioModel.get().getContrasenyaUsuario().equals(contrasenyaUsuario)) {
            return usuarioModel;
        }

        return Optional.empty();
    }

    public boolean updateUsuario(long idUsuario, String contrasenyaUsuario, String nuevaContrasenya) {
        Optional<UsuarioModel> usuarioModel = comprobarUsuario(idUsuario, contrasenyaUsuario);

        if (usuarioModel.isPresent()) {
            usuarioModel.get().setContrasenyaUsuario(nuevaContrasenya);
            usuarioRepository.save(usuarioModel.get());
            return true;
        }

        return false;
    }

    public boolean deleteUsuario(long idUsuario, String contrasenyaUsuario) {
        try {
            Optional<UsuarioModel> usuarioModel = comprobarUsuario(idUsuario, contrasenyaUsuario);

            if (usuarioModel.isPresent()) {
                // TODO: Borrar en cascada
                usuarioRepository.deleteById(idUsuario);
                return true;
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return false;
    }
}
