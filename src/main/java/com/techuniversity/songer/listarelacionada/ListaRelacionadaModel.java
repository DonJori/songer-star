package com.techuniversity.songer.listarelacionada;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="listarelacionada")
public class ListaRelacionadaModel {

    @Transient
    public static final String SEQUENCE_NAME = "listas_sequence";

    @Id
    private long id;
    private long idLista;
    private long idCancion;

    public ListaRelacionadaModel() {}

    public ListaRelacionadaModel(long id, long idLista, long idCancion) {
        this.id = id;
        this.idLista = idLista;
        this.idCancion = idCancion;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdLista() {
        return idLista;
    }

    public void setIdLista(long idLista) {
        this.idLista = idLista;
    }

    public long getIdCancion() {
        return idCancion;
    }

    public void setIdCancion(long idCancion) {
        this.idCancion = idCancion;
    }
}
