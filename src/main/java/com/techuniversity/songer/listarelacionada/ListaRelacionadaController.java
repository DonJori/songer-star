package com.techuniversity.songer.listarelacionada;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/lista")
public class ListaRelacionadaController {

    @Autowired
    ListaRelacionadaService listaRelacionadaService;

    @GetMapping("/")
    public List<ListaRelacionadaModel> getLista(@RequestParam(defaultValue = "-1") String page) {
        int iPage = Integer.parseInt(page);
        if (iPage == -1) {
            return listaRelacionadaService.findAll();
        } else {
            return listaRelacionadaService.findPaginado(iPage);
        }
    }

    @GetMapping("/filtrar")
    public List<?> getIdLista(@RequestParam long idLista) {
        return listaRelacionadaService.filtrarPoridLista(idLista);
    }

    @PostMapping("/")
    public ListaRelacionadaModel insertLista(@RequestBody ListaRelacionadaModel lista) throws Exception {
        return listaRelacionadaService.insertarListaRelacionada(lista);
    }

    @PutMapping("/")
    public void updateLista(@RequestBody ListaRelacionadaModel lista) {
        listaRelacionadaService.update(lista);
    }

    @DeleteMapping("/{idUsuario}/{idLista}/{idCancion}")
    public boolean deleteListaCancion(@PathVariable long idUsuario,@PathVariable long idLista,@PathVariable long idCancion,@RequestParam String contrasenyaUsuario) {

        return listaRelacionadaService.deleteListaCancion(idUsuario, idLista, idCancion, contrasenyaUsuario);
    }

    @DeleteMapping("/{idUsuario}/{idLista}")
    public boolean deleteLista(@PathVariable long idUsuario,@PathVariable long idLista,@RequestParam String contrasenyaUsuario) {

        return listaRelacionadaService.deleteLista(idUsuario, idLista, contrasenyaUsuario);
    }
}
