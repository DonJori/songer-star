package com.techuniversity.songer.listarelacionada;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import com.techuniversity.songer.usuario.UsuarioModel;
import com.techuniversity.songer.utils.SequenceGeneratorService;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.mongodb.client.model.Filters.eq;

@Service

public class ListaRelacionadaService {

    @Autowired
    ListaRelacionadaRepository listaRelacionadaRepository;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    private static MongoCollection<Document> getServiciosCollection() {
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder().applyConnectionString(cs).retryWrites(true).build();
        MongoClient cliente = MongoClients.create(settings);
        MongoDatabase database = cliente.getDatabase("songerStar");
        return database.getCollection("listarelacionada", Document.class);
    }

    public List<ListaRelacionadaModel> findAll() {
        return listaRelacionadaRepository.findAll();
    }

    public Optional<ListaRelacionadaModel> findByid(long id) {
        return listaRelacionadaRepository.findById(id);
    }

    public List<ListaRelacionadaModel> filtrarPoridLista (long idLista) {
        MongoCollection<Document> relaciones = getServiciosCollection();

        List<ListaRelacionadaModel> listaRelacionadaModel= new ArrayList<>();
        Document query = new Document("idLista", new Document("$eq", idLista));
        ArrayList<Document> ocurrenciasRelaciones = relaciones.find(query).into(new ArrayList<Document>());

        for (Document document : ocurrenciasRelaciones) {
            ListaRelacionadaModel instancia = new ListaRelacionadaModel();

            if (document.get("_id") != null) {
                long id = ((Number)document.get("_id")).longValue();
                instancia.setId(id);
            }

            if (document.get("idLista") != null) {
                instancia.setIdLista(((Number)document.get("idLista")).longValue());
            }

            if (document.get("idCancion") != null) {
                instancia.setIdCancion(((Number)document.get("idCancion")).longValue());
            }
            listaRelacionadaModel.add(instancia);
        }

        return listaRelacionadaModel;
    }

    public ListaRelacionadaModel update(ListaRelacionadaModel listacanciones) {
        return listaRelacionadaRepository.save(listacanciones);
    }

    /*
    public boolean deleteLista (ListaRelacionadaModel listacanciones) {
        try {
            listaRelacionadaRepository.delete(listacanciones);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }*/

    public List<ListaRelacionadaModel> findPaginado(int page) {
        Pageable pageable = PageRequest.of(page, 3);
        Page<ListaRelacionadaModel> pages = listaRelacionadaRepository.findAll(pageable);
        List<ListaRelacionadaModel> canciones = pages.getContent();
        return canciones;
    }

    public ListaRelacionadaModel insertarListaRelacionada(ListaRelacionadaModel listaRelacionadaModel) throws Exception {

        ListaRelacionadaModel nuevaLista = new ListaRelacionadaModel(
                sequenceGeneratorService.generateSequence(ListaRelacionadaModel.SEQUENCE_NAME),
                listaRelacionadaModel.getIdLista(),
                listaRelacionadaModel.getIdCancion());

        return listaRelacionadaRepository.save(nuevaLista);
    }

    public boolean deleteListaCancion(long idUsuario, long idLista, long idCancion, String contrasenyaUsuario) {

        List<ListaRelacionadaModel> listaRelaciones = filtrarPoridLista(idLista);
        ListaRelacionadaModel objetivo = null;
        for (ListaRelacionadaModel dummy :listaRelaciones) {
            if (dummy.getIdCancion()==idCancion) {
                objetivo = dummy;
                break;
            }
        }
        if (objetivo != null) {
            listaRelacionadaRepository.delete(objetivo);
            return true;
        }
        return false;
    }

    public boolean deleteLista(long idUsuario, long idLista, String contrasenyaUsuario) {
        List<ListaRelacionadaModel> listaRelaciones = filtrarPoridLista(idLista);
        for (ListaRelacionadaModel dummy :listaRelaciones) {
            listaRelacionadaRepository.delete(dummy);
        }
        return true;
    }
}
