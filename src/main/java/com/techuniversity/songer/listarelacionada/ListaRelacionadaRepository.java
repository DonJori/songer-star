package com.techuniversity.songer.listarelacionada;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ListaRelacionadaRepository extends MongoRepository<ListaRelacionadaModel, Long> {


}
